#include<stdio.h>
#include<math.h>
float get_radius()
{
	float radius;
	printf("enter the radius:");
	scanf("%f", &radius);
	return radius;
}

float compute_area(float radius)
{
	return M_PI * radius * radius;
}

float circum(float radius)
{
	return 2 * M_PI * radius;
}

void output(float radius, float area)
{
	printf("radius and area of a circle=%fis%f\n", radius, area);
}

void output_circumference(float radius, float circumf)
{
	printf("circumference of a circle radius=%fis%f\n", radius, circumf);
}

int main()
{
	float radius, area, circumference;
	radius = get_radius();
	area = compute_area(radius);
	circumference = circum(radius);
	output(radius, area);
	output_circumference(radius, circumference);
	return 0;
}